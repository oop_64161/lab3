import java.util.Scanner;

public class Problem4 {
    public static void main(String[] args) {
        int Num, Sum = 0, round = 0;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.print("Plase input number : ");
            Num = sc.nextInt();
            if (Num != 0) {
                round++;
                Sum = Sum + Num;
                System.out.println("Sum: " + Sum + ", Avg: " + (((double) Sum) / round));
            }
        } while (Num != 0);
        System.out.println("Bye");
        sc.close();
    }
}
