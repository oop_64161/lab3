import java.util.Scanner;

public class Problem5 {
    public static void main(String[] args) {
        String str;
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Plase input : ");
            str = sc.nextLine();
            if (str.trim().equalsIgnoreCase("Bye")) {
                System.out.println("Exit Program");
                System.exit(0);
            }
            System.out.println(str);
        }
    }
}
