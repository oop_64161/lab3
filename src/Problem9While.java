import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        int Num, i = 1;
        Scanner sc = new Scanner(System.in);
        System.out.print("Plase input n: ");
        Num = sc.nextInt();
        while (i <= Num) {
            i++;
            int j = 1;
            while (j <= Num) {
                System.out.print(j);
                j++;
            }
            System.out.println();
        }
        sc.close();
    }
}
